<?php
function get_posts($limit, $offset): array
{
    global $link;

    settype($limit, 'integer');
    settype($offset, 'integer');

    $sql = "SELECT * FROM `posts` ORDER BY `posts`.`created_at` DESC LIMIT $limit OFFSET $offset;";
    $result = mysqli_query($link, $sql);

    return mysqli_fetch_all($result, MYSQLI_ASSOC);
}

function get_post_by_id($post_id): bool|array|null
{
    global $link;

    settype($post_id, 'integer');

    $sql = 'SELECT * FROM `posts` WHERE id = '.$post_id;
    $result = mysqli_query($link, $sql);

    return mysqli_fetch_assoc($result);
}

function get_comments_by_postid($post_id): array
{
    global $link;

    settype($post_id, 'integer');

    $sql = 'SELECT * FROM comments INNER JOIN users ON comments.user_id = users.id WHERE post_id = '.$post_id;
    $result = mysqli_query($link, $sql);

    return mysqli_fetch_all($result, MYSQLI_ASSOC);
}

//старт сессии
session_start();

// переменные по умолчанию
$login = "";
$errors = array();

// подключение БД из глобальной переменной в database.php
global $link;

// регистрация пользователя
if (isset($_POST['reg_user'])) {
    // получению данных из POST запроса формы регистрации
    $login = mysqli_real_escape_string($link, $_POST['login']);
    //mysqli_real_escape_string для экранирования
    $password_1 = mysqli_real_escape_string($link, $_POST['password_1']);
    $password_2 = mysqli_real_escape_string($link, $_POST['password_2']);

    // валидация формы
    // через массив errors потом можно проверить че по ошибкам
    if (empty($login)) {
        $errors[] = "Login is required";
    }
    if (empty($password_1)) {
        $errors[] = "Password is required";
    }
    if ($password_1 != $password_2) {
        $errors[] = "The two passwords do not match";
    }

    // проверка существует ли уже пользователей
    $user_check_query = "SELECT * FROM users WHERE login='$login' LIMIT 1";
    $result = mysqli_query($link, $user_check_query);
    $user = mysqli_fetch_assoc($result);

    if ($user) {
        if ($user['login'] === $login) { // remove
            $errors[] = "Login already exists";
        }
    }

    // Если пользователя нет и ошибок нет
    if (count($errors) == 0) {
        $password = md5($password_1); //хеширование пароля по md5

        //добавление пользователя в БД
        $query = "INSERT INTO users (login, password) 
  			  VALUES('$login', '$password')";
        mysqli_query($link, $query);
        //добавление значения логина в сессию
        $_SESSION['login'] = $login;
    }
}

// авторизация пользователя
if (isset($_POST['login_user'])) {
    $login = mysqli_real_escape_string($link, $_POST['login']);
    $password = mysqli_real_escape_string($link, $_POST['password']);

    if (empty($login)) {
        $errors[] = "Login is required";
    }
    if (empty($password)) {
        $errors[] = "Password is required";
    }

    if (count($errors) == 0) {
        $password = md5($password);
        $query = "SELECT * FROM users WHERE login='$login' AND password='$password'";
        $results = mysqli_query($link, $query);
        if (mysqli_num_rows($results) == 1) {
            $_SESSION['login'] = $login;

        } else {
            $errors[] = "Wrong login/password combination";
        }
    }
}


if (isset($_POST['add_comment'])) {
    $content = mysqli_real_escape_string($link, $_POST['content']);
    $post = $_GET['post_id'];

    global $link;
    $login = $_SESSION['login'];

    $sql = "SELECT * FROM users WHERE login='$login'";
    $result = mysqli_query($link, $sql);
    $user = mysqli_fetch_assoc($result);
    $user_id = $user['id'];



    $query = "INSERT INTO comments (content, user_id, post_id) VALUES('$content', '$user_id' , '$post')";
    mysqli_query($link, $query);

}


// восстановление пароля ЗАПРОС ССЫЛКИ
if(isset($_POST['reset-pass-req'])) {
    global $link;
    $login = $_POST['reset_pass_login'];

    $sql = "SELECT * FROM users WHERE login='$login'";
    $result = mysqli_query($link, $sql);
    $user = mysqli_fetch_assoc($result);

    $key = md5($user['password']);
    var_dump($key);

    $sql = "UPDATE users SET reset_key = " . "'$key'" . " WHERE login='$login'";
    mysqli_query($link, $sql);
}

// восстановление пароля ОБРАБОТКА
if(isset($_POST['reset-pass-process'])) {
    global $link;

    $login = $_POST['login'];


    if($_POST['password_1'] == $_POST['password_2']) {
        $password = md5($_POST['password_2']);
        $sql = "UPDATE users SET password = " . "'$password'" . " WHERE login='$login'";
        mysqli_query($link, $sql);
        $success = true;
    } else {
        $success = false;
    }

}