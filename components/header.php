<?php
include 'include/database.php';
include 'include/functions.php';



if (isset($_GET['logout'])) {
    session_destroy();
    $_SESSION = [];
}
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dalee blog test</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <link rel="stylesheet" href="../static/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
          rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="../media/favicon.ico"/>
    <script src="https://kit.fontawesome.com/07906dadd2.js" crossorigin="anonymous"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light ms-5">
    <img src="../media/favicon.ico" alt="Dalee" class="navbar-brand">
    <a class="navbar-brand p-lg-3" href="../index.php">Dalee Blog</a>

    <div class="collapse navbar-collapse ms-5" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item"><a class="nav-link" aria-current="page" href="../index.php">Главная</a>

                <?php
                if (!isset($_SESSION['login'])) {
                ?>
            <li class="nav-item"><a class="nav-link" aria-current="page" href="../login.php">Авторизация</a> <?php
                }
                ?>

                <?php
                if (isset($_SESSION['login'])) {
                ?>
            <li class="nav-item"><a class="nav-link" aria-current="page" href="../index.php?logout='1'">Выйти</a> <?php
                }
                ?>
        </ul>

        <!--        <span class="navbar-text me-lg-5">-->
        <!--                Добро пожаловать, | <a class="logout" href="#">выход</a>-->
        <!--        </span>-->
    </div>
</nav>