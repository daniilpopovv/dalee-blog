<?php
require_once('components/header.php');
$post_id = $_GET['post_id'] ?? null;

$post = get_post_by_id($post_id);

if (!$post) {
    http_response_code(404);
    die();
}

$comments = get_comments_by_postid($post_id);

if ($comments)

?>

<div class="container mt-5">
    <div class="card mb-3" style="max-width: 1000px;">
        <div class="row g-0">
            <?php
            if (isset($post['image'])) {
                ?>
                <div class="col-md-3">
                    <img src="media/<?= $post['image'] ?? '' ?>" class="img-fluid rounded-start" alt="<?= $post['image']?>">
                </div>
                <?php
            }
            ?>
            <div class="col-md-9">
                <div class="card-body">
                    <h1 class="page-header"><?= $post['title'] ?? '' ?></h1>
                    <p class="card-text"><?= $post['description'] ?? '' ?></p>
                    <p class="card-text"><i class="fa-regular fa-calendar"></i> <?= $post['created_at'] ?? '' ?></p>
                </div>
            </div>
        </div>
    </div>
    <h3>Комментарии</h3>
    <?php foreach ($comments as $comment): ?>
        <div class="card mb-3" style="max-width: 700px;">
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title"><?= $comment['login'] ?></h5>
                    <p class="card-text"><?= $comment['content'] ?></p>
                </div>
            </div>
        </div>
    <?php endforeach; ?>

    <form method="post" action="post.php?post_id=<?= $post['id'] ?>" class="col-md-5">
        <input type="hidden" name="post_id" value="<?= $post['id'] ?>">
        <div class="mb-3">
            <label for="comment-content" class="form-label">Оставить комментарий</label>
            <textarea type="text" name="content" id="comment-content" class="form-control">
            </textarea>
        </div>
        <div class="mb-3">
            <button type="submit" class="btn btn-primary" name="add_comment">Опубликовать</button>
        </div>
    </form>
</div>

<?php
require_once 'components/footer.php';
?>

