<?php
require_once('components/header.php');
global $success;
?>

<?php
if ($success) {
    ?>
<h1>Пароль успешно изменен!</h1>
<?php
} else {
    ?>
<h1>Ошибка при заполнении паролей</h1>
<?php
}
?>

<?php
require_once 'components/footer.php';
?>

