<?php
require_once('components/header.php');
?>

<div class="container mt-5" style="max-width: 30%">
    <h1>Авторизация</h1>

    <form method="post" action="login.php">
        <div class="mb-3">
            <label for="auth-login" class="form-label">Логин</label>
            <input class="form-control" type="text" name="login" id="auth-login">
        </div>
        <div class="mb-3">
            <label for="auth-pass" class="form-label">Пароль</label>
            <input class="form-control" type="password" name="password" id="auth-pass">
        </div>
        <div class="mb-3">
            <button type="submit" class="btn btn-primary" name="login_user">Войти</button>
        </div>
        <p>Не зарегистрированы? <a href="register.php">Регистрация</a></p>
    </form>
</div>

<div class="container mt-5" style="max-width: 30%">
    <h5>Забыли пароль? Ну не беда. Введите логин, чтобы восстановить</h5>

    <form method="post" action="reset-password.php">
        <div class="mb-3">
            <label for="reset-pass-login" class="form-label">Вот сюда, да</label>
            <input class="form-control" type="text" name="reset_pass_login" id="reset-pass-login">
        </div>
        <div class="mb-3">
            <button type="submit" class="btn btn-primary" name="reset-pass-req">Восстановить пароль</button>
        </div>
    </form>
</div>

<?php
require_once 'components/footer.php';
?>

