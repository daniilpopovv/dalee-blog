<?php
require_once('components/header.php');

global $link;

$key = $_GET['key'];
$sql = "SELECT * FROM users WHERE reset_key='$key'";
$result = mysqli_query($link, $sql);
$user = mysqli_fetch_assoc($result);
$login = $user['login'];
?>
<?php
if (isset($user)) {
    ?>
    <div class="container mt-5" style="max-width: 30%">
        <h1>Восстановление пароля</h1>

        <form method="post" action="confirm.php">
            <div class="mb-3">
                <input type="hidden" name="login" value="<?=$login ?>">
                <label for="register-password-1" class="form-label">Новый пароль</label>
                <input class="form-control" type="password" name="password_1" id="register-password-1" required>
            </div>
            <div class="mb-3">
                <label for="register-password-2" class="form-label">Подтвердите пароль</label>
                <input class="form-control" type="password" name="password_2" id="register-password-2" required>
            </div>
            <div class="mb-3">
                <button type="submit" class="btn btn-primary" name="reset-pass-process">Сменить пароль</button>
            </div>
        </form>
    </div>

<?php
} else {
    ?>
    <h1>Ссылка неверна</h1>

<?php
}
?>

<?php
require_once 'components/footer.php';
?>

