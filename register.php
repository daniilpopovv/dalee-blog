<?php
require_once('components/header.php');
?>
<div class="container mt-5" style="max-width: 30%">
    <h1>Регистрация</h1>
    <form method="post" action="register.php">
        <div class="mb-3">
            <label for="register-login" class="form-label">Логин</label>
            <input class="form-control" type="text" name="login" id="register-login">
        </div>
        <div class="mb-3">
            <label for="register-password-1" class="form-label">Пароль</label>
            <input class="form-control" type="password" name="password_1" id="register-password-1">
        </div>
        <div class="mb-3">
            <label for="register-password-2" class="form-label">Подтвердите пароль</label>
            <input class="form-control" type="password" name="password_2" id="register-password-2">
        </div>
        <div class="mb-3">
            <button type="submit" class="btn btn-primary" name="reg_user">Зарегистрироваться</button>
        </div>
        <p>
            Уже зарегистрированы? <a href="login.php">Войти</a>
        </p>
    </form>
</div>


<?php
require_once 'components/footer.php';
?>

