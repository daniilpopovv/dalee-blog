<?php require_once('components/header.php'); ?>

    <div class="container mt-5">
        <?php
        $page = $_GET['page'] ?? 1;
        $limit = 5;
        $offset = $limit * ($page - 1);
        $posts = get_posts($limit, $offset);

        if (intdiv(count(get_posts(1000, 0)), 5) == 0) {
            $all_pages = 1;
        }
        if (count(get_posts(1000, 0)) % 5 != 0) {
            $all_pages = intdiv(count(get_posts(1000, 0)), 5) + 1;
        }
        ?>
        <?php foreach ($posts as $post): ?>
            <div class="card mb-3" style="max-width: 700px;">
                <div class="row g-0">
                    <?php
                    if (isset($post['image'])) {
                        ?>
                        <div class="col-md-3">
                            <img src="media/<?= $post['image'] ?>" class="img-fluid rounded-start"
                                 alt="<?= $post['image'] ?>">
                        </div>
                        <?php
                    }
                    ?>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= $post['title'] ?></h5>
                            <p class="card-text"><?= mb_substr($post['description'], 0, 50, 'UTF-8') . '...' ?></p>
                            <p class="card-text"><i class="fa-regular fa-calendar"></i> <?= $post['created_at'] ?></p>
                            <a href="post.php?post_id=<?= $post['id'] ?>" class="btn btn-danger">Читать полностью</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <?php
                if ($page != 1) {
                    ?>
                    <li class="page-item"><a class="page-link" href="/?page=<?= $page - 1 ?>">Previous</a></li>
                    <?php
                }
                ?>

                <?php
                for ($i = 1; $i <= $all_pages; $i++) {
                    ?>
                    <li class="page-item"><a class="page-link" href="/?page=<?= $i ?>"><?= $i ?></a></li>
                    <?php
                }
                ?>

                <?php
                if ($page != $all_pages and $all_pages != 0) {
                    ?>
                    <li class="page-item"><a class="page-link" href="/?page=<?= $page + 1 ?>">Next</a></li>
                    <?php
                }
                ?>

            </ul>
        </nav>
    </div>

<?php require_once('components/footer.php'); ?>